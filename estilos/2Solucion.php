<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>

            .centrado{
                margin-left:auto;
                margin-right: auto;
            }

            .caja1{
                width:200px;
                color:white;
            }

            .caja2{
                width:400px;
                font-size: 20px;
                color:white;
                padding:20px;
            }

            .rojo{
                background-color: red;
            }

            .azul{
                background-color: blue;
            }

            img{
                width:300px;
            }

            .caja3{
                width:910px;
            }
        </style>
    </head>
    <body>
        <br>
        <div class="caja1 rojo centrado">
            Este div colocarle centrado en la pagina con un fondo rojo
            y texto blanco.
            Colocarle una anchura de 200px
        </div>
        <br>
        <div class="azul caja2 centrado">
            Este div colocarle centrado en la pagina con un fondo azul.
            Texto blanco y con una tamaño de letra de 20px
            Colocarle una anchura de 400px
            Colocarle unos margenes internos (padding) de 20px
        </div>
        <br>
        <div class="centrado caja3">
            <!--
                Colocar 3 fotos con una anchura cada una de 300px
                La caja que contiene las fotos debe estar centrada en la pagina
            -->
            <img src="imgs/1.jpg" />
            <img src="imgs/2.jpg" />
            <img src="imgs/3.jpg" />
        </div>

    </body>
</html>
