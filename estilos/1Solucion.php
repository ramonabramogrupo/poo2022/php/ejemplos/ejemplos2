<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            .separaciones{
                margin:10px 5px;
            }

            .margenes{
                padding:10px;
            }

            .textoCentrado{
                text-align: center;
            }

            .textoJustificado{
                text-align: justify;
            }

            .centrarCaja{
                margin-left:auto;
                margin-right: auto;
            }

            .ancho1{
                width:100px;
            }

            .ancho2{
                width:200px;
            }

            .ancho3{
                width:300px;
            }

            .centrarImagen{
                margin-left:auto;
                margin-right: auto;
                display:block;
            }

            .colorLetra1{
                color:white;
            }

            .colorLetra2{
                color:black;
            }

            .fondo1{
                background-color:white;
            }

            .fondo2{
                background-color:black;
            }
            .redondear{
                border-radius: 20px;
            }

        </style>
    </head>
    <body>
        <div class="textoCentrado fondo2 colorLetra1 margenes separaciones">
            Quiero este texto centrado y con un fondo negro y
            color de letra blanca.
            Colocar margenes y separaciones
        </div>
        <br>
        <div class="textoCentrado fondo2 colorLetra1 margenes centrarCaja ancho3">
            Quiero este texto centrado y con un fondo negro y
            color de letra blanca.
            Ademas quiero una anchura de 300px
            Colocar margenes.
            Colocar la caja centrada
        </div>
        <br>
        <div>
            <!-- Colocar aqui una imagen centrada con una anchura de 200px -->
            <img src="imgs/1.jpg" class="centrarImagen ancho2">
        </div>
        <br>
        <div>
            <!-- Colocar aqui otra imagen centrada con una anchura de 300px -->
            <img src="imgs/2.jpg" class="centrarImagen ancho3">
        </div>
        <br>
        <div>
            <!-- Colocar aqui otra imagen centrada con una anchura de 200px y redondeados los bordes -->
            <img src="imgs/3.jpg" class="centrarImagen ancho2 redondear">
        </div>

    </body>
</html>
