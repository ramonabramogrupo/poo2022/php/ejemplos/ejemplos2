<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo</title>
    </head>
    <body>
        <div>
            <img src="https://picsum.photos/600/400?random=1" alt="">
        </div>

        <?php
        // Colocar con codigo php 5 fotos
        // cada una en un div distinto
        echo "<div>";
        echo '<img src="https://picsum.photos/600/400?random=2" alt="">';
        echo "</div>";
        echo "<div>";
        echo '<img src="https://picsum.photos/600/400?random=3" alt="">';
        echo "</div>";
        echo "<div>";
        echo '<img src="https://picsum.photos/600/400?random=4" alt="">';
        echo "</div>";
        echo "<div>";
        echo '<img src="https://picsum.photos/600/400?random=5" alt="">';
        echo "</div>";
        echo "<div>";
        echo '<img src="https://picsum.photos/600/400?random=6" alt="">';
        echo "</div>";
        ?>


    </body>
</html>