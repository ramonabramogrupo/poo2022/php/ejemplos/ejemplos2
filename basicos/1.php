<?php
// Añadir un div con un parrafo utilizando echo
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo</title>
    </head>
    <body>
        <div>
            <p>Ejemplo</p>
        </div>
        <?php
        // añadir aqui el div con el parrafo como el anterior
        ?>
    </body>
</html>
