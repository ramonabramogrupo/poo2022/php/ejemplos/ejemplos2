<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Document</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    </head>
    <body>
        <?php
        // Tenemos una nota y queremos saber
        // que nota tiene en texto
        // si es menor que 5 suspenso
        // si esta entre 5 y 7 aprobado
        // si esta entre 7 y 9 notable
        // si esta entre 9 y 10 sobresaliente
        // si es 10 matricula
        // debeis utilizar un if

        $nota = 6;
        ?>
        <!--
        mx-auto : centrar la tabla
        table: formato de tabla
        table-dark: tabla en oscuro
        table-bordered: tabla con bordes
        table-striped: tabla con bandas en filas
        col-3: utiliza 3 columnas de 12
        -->
        <table class="my-2 mx-auto table table-dark table-bordered table-striped col-3">
            <thead>
                <tr>
                    <th>Rango</th>
                    <th>Valor</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><5</td>
                    <td>Suspenso</td>
                </tr>
                <tr>
                    <td>5-7</td>
                    <td>Aprobado</td>
                </tr>
                <tr>
                    <td>7-9</td>
                    <td>Notable</td>
                </tr>
                <tr>
                    <td>9-10</td>
                    <td>Sobresaliente</td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Matricula</td>
                </tr>
            </tbody>
        </table>

        <div class="card bg-primary text-white col-4 mx-auto">
            <div class="card-body">
                <?php
                // aqui debe mostrar la nota numerica
                ?>
            </div>
        </div>
        <br>
        <div class="card bg-primary text-white col-4 mx-auto">
            <div class="card-body">
                <?php
                // aqui debe mostrar la nota en texto
                ?>
            </div>
        </div>
    </body>
</html>

