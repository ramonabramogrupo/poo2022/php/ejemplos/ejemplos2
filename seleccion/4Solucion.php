<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Ejemplo</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
    </head>
    <body>
        <?php
        // si aprueba debe colocar la imagen aprobado
        // si suspende debe colocar la imagen suspenso
        // la eqtiqueta la vamos a colocar asi
        // <img style="width:200px" class="mt-4 mx-auto d-block" src="">
        // mt-4 : separar la imagen de la parte superior
        // mx-auto d-block : centrar las imagenes

        $nota = 6;
        if ($nota < 5) {
            echo '<img style="width:200px" class="mt-4 mx-auto d-block" src="imgs/suspenso.png">';
        } else {
            echo '<img style="width:200px" class="mt-4 mx-auto d-block" src="imgs/aprobado.png">';
        }
        ?>
    </body>
</html>
