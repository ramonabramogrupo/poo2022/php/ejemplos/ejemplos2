<?php

$nota = 4;
const APROBADO = 5;

// indicar si la nota es aprobado o suspenso
// para aprobar hay que sacar una nota por encima o igual al aprobado

$salida = "suspenso";
if ($nota >= APROBADO) {
    $salida = "aprobado";
}

echo $salida;

