<?php
$numero1 = 10;
$numero2 = 20;
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // indicar cual de los dos numeros es mayor utilizando if
        if ($numero1 > $numero2) {
            echo "El mayor es {$numero1}";
        } else {
            echo "El mayor es {$numero2}";
        }
        ?>
    </body>
</html>
